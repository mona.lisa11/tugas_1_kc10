from django import forms
from .models import PostModel

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = {
            'nama',
            'password',
        }

        widgets = {
            'nama' : forms.TextInput(
                attrs={
					'class':'form-control',
                }
            ),
			
			'password' : forms.TextInput(
                attrs={
					'class':'form-control',
                }
            ),
        }
