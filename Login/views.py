from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PostForm
from .models import PostModel

def index(request):
    post_form = PostForm(request.POST or None)
    if request.method == "POST":
        if post_form.is_valid():
            post_form.save()
            print(post_form.data)  
            return redirect('berhasil')

    context = {
        'page_title':'Create Post',
        'post_form': post_form,
    }
    
    return render(request, 'Login.html', context)

def create(request):
    return render(request, "success.html")
