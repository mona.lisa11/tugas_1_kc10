from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import create
from django.http import HttpRequest
import unittest


# Create your tests here.

class Lab1UnitTest(TestCase):

    def test_tagline_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, index)

    def test_using_create_func(self):
        found = resolve('/login/success/')
        self.assertEqual(found.func, create)
