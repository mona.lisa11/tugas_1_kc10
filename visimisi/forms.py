from django.db import core.models
from datetime import datetime, date
from django import forms
from .models import Election
from .models import party
from .models import Candidate

class FormPage(forms.Form):
    attrs = {
        'class':'form-control'
    }

    title = forms.CharField(label='Judul',max_length=200,blank=False)
    polling_time= forms.DateField(blank=True. null=True)
    name= forms.CharField(label='Nama', max_length=30, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    party= forms.CharField(label='Nama Partai', max_length=3 0,required=True, widget=forms.TextInput(attrs={'class':form}))
    election = forms.CharField(label='Pemilihan', max_length=3 0,required=True, widget=forms.TextInput(attrs={'class':form}))
    number = forms.NumberInput(label='Nomor Pemilihan',max_length=3)
    full_name  = forms.CharField(label='Nama Lengkap', max_length=100, required=True,widget=forms.TextInput(attrs={'class':form})
    motto = forms.CharField(label='Misi',required=True, widget=forms.TextInput(attrs={'class':form}))
    datebirth = forms.DateField(label = 'Tangal Lahir',required=True,widget=forms.DateInput(attrs={'type':'date'}))
    vision =forms.CharField(label='Visi',required=True, widget=forms.TextInput(attrs={'class':form})
    mission = forms.CharField(label='Misi',required=True, widget=forms.TextInput(attrs={'class':form}))
    username = forms.CharField(label='Email', max_length=100, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label ='Password',max_length = 50, required=True, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    file_ktp =  forms.FileField(label ='Masukan KTP'widget=forms.ClearableFileInput(attrs={'multiple': True}))