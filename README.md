# Tugas 1 PPW - KC10
***

#Member:
***
    1. Erania Rajisa
    2. Muhamad Nicky
    3. Monalisa Merauje
    4. Inigo Ramli
    5. Yobelio Ekaharja Putra
    
#Link Heroku: https://untuk-rakyat.herokuapp.com/
***

#Deskripsi Web:
***
    Web ini sebagai salah satu sarana bagi para aktifis politik yang merupakan calon wakil rakyat, 
    dapat berkampanye dengan  memanfaatkan  teknologi digital yang ada, sehingga rakyat dapat dengan mudah 
    mengenali atau mengikuti perkembangan dari calon yang akan maju dalam pesta demokrasi nantinya

#Daftar Fitur Web:
***
    1. log in
    2. sign in
    3. fisi misi calon
    4. perbandingan antar calon
    5. searching nama calon
