from django.test import TestCase
from django.urls import reverse
from core.models import Election, Party, Candidate

from datetime import datetime, date, timedelta

# Create your tests here.

class ElectionArchiveTest(TestCase):
    def setUp(self):
        Election.objects.create(title="Pemira IKM Fasilkom", polling_time=datetime.now() + timedelta(days=10))
        Election.objects.create(title="The Next Kak Pewe 2019", polling_time=datetime.now() + timedelta(days=5))
        Election.objects.create(title="The Next Kak Pewe 2018", polling_time=datetime.now() - timedelta(days=10))
        Election.objects.create(title="New Election without clear date")

    def test_election_archive_success(self):
        response = self.client.get(reverse('election_archive'))

        self.assertEqual(response.status_code, 200)
        election_list = response.context.get('election_list', [])
        self.assertEqual(len(election_list), 4)

    def test_election_archive_order(self):
        response = self.client.get(reverse('election_archive'))
        election_list = response.context.get('election_list', [])

        self.assertEqual(election_list[0].title, "The Next Kak Pewe 2019")
        self.assertEqual(election_list[1].title, "Pemira IKM Fasilkom")
        self.assertEqual(election_list[2].title, "New Election without clear date")
        self.assertEqual(election_list[3].title, "The Next Kak Pewe 2018")

class CandidateArchiveTest(TestCase):
    def setUp(self):
        Election.objects.create(title="Pemira IKM Fasilkom")
        Election.objects.create(title="The Next Kak Pewe")

        Party.objects.create(name="Independen")
        Party.objects.create(name="Partai Kak Pewe (PKP)")

        Candidate.objects.create(election_id=1,
            party_id=2,
            number=1,
            full_name='Eran',
            motto='Hai',
            date_of_birth=datetime(1999, 5, 15),
            username='Eran',
            password='pass1')

        Candidate.objects.create(election_id=1,
            party_id=1,
            number=2,
            full_name='Monalisa',
            motto='Yang penting jadi',
            date_of_birth=datetime(1999, 10, 10),
            username='Mona456',
            password='password')

        Candidate.objects.create(election_id=1,
            party_id=1,
            number=1,
            full_name='Yobelio',
            motto='Semoga langgeng',
            date_of_birth=datetime(2000, 1, 1),
            username='Yobel123',
            password='123456')

        Candidate.objects.create(election_id=2,
            party_id=1,
            number=1,
            full_name='Inigo Ramli',
            motto='Bagus',
            date_of_birth=datetime(2000, 9, 8),
            username='IgoRamli',
            password='pass')

    def test_candidate_archive_success_no_param(self):
        response = self.client.get(reverse('candidate_archive', kwargs={'election_id': 1}))

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 3)

    def test_candidate_archive_success_order(self):
        response = self.client.get(reverse('candidate_archive', kwargs={'election_id': 1}))

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(candidate_list[0].full_name, 'Yobelio')
        self.assertEqual(candidate_list[1].full_name, 'Monalisa')
        self.assertEqual(candidate_list[2].full_name, 'Eran')

    def test_candidate_archive_success_name_param(self):
        uri = reverse('candidate_archive', kwargs={'election_id': 1})
        response = self.client.get(uri, {'name_contains': 'Yob'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 1)

    def test_candidate_archive_sucess_party_param(self):
        uri = reverse('candidate_archive', kwargs={'election_id': 1})
        response = self.client.get(uri, {'party_contains': 'PKP'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 1)
        self.assertEqual(candidate_list.first().full_name, 'Eran')

    def test_candidate_archive_sucess_name_and_party_param(self):
        uri = reverse('candidate_archive', kwargs={'election_id': 1})
        response = self.client.get(uri, {'name_contains': 'Yob', 'party_contains': 'PKP'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 0)

    def test_candidate_archive_fail_no_such_election(self):
        response = self.client.get(reverse('candidate_archive', kwargs={'election_id': 3}))
        self.assertEqual(response.status_code, 404)

    def test_candidate_archive_success_post_request(self):
        # POST is fine, but the request content will be ignored
        uri = reverse('candidate_archive', kwargs={'election_id': 1})
        response = self.client.post(uri, {'party_contains': 'PKP'})

        self.assertEqual(response.status_code, 200)
        candidate_list = response.context.get('candidate_list', [])
        self.assertEqual(len(candidate_list), 3)