from django.urls import path

from . import views

urlpatterns = [
    path('', views.election_archive, name='election_archive'),
    path('<int:election_id>', views.candidate_archive, name='candidate_archive'),
]
