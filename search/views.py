from django.shortcuts import render
from django.http import *
from itertools import chain

from core.models import Election, Candidate

from datetime import datetime, date

# Create your views here.

def election_archive(request):
	# Election ordering:
	# 1. Upcoming / TBA Election precedes Past Election
	# 2. For Upcoming Election, Earlier Election precedes Later Election
	# 3. For Past Election, Later Election precedes Earlier Election
	# 3. TBA Election precedes Past Election and succedes Upcoming Election
	election_list_1 = Election.objects.filter(polling_time__gte=datetime.now()).order_by('polling_time')
	election_list_null = Election.objects.filter(polling_time__isnull=True).order_by('polling_time')
	election_list_2 = Election.objects.filter(polling_time__lt=datetime.now()).order_by('-polling_time')

	context = {'election_list': list(chain(election_list_1, election_list_null, election_list_2))}
	return render(request, 'election-archive.html', context)

def candidate_archive(request, election_id):
	# Check if election with id election_id exists
	if not Election.objects.filter(id=election_id).exists():
		return HttpResponseNotFound()

	if request.method == 'GET':
		# Get filtered result
		data = request.GET
		name_param = data.get('name_contains', '')
		party_param = data.get('party_contains', '')
		candidate_list = Candidate.objects.filter(
			election_id=election_id,
			full_name__contains=name_param,
			party__name__contains=party_param).order_by('party', 'number')
	else:
		# Return all candidates
		candidate_list = Candidate.objects.filter(
			election_id=election_id)

	context = {'election': Election.objects.get(id=election_id), 'candidate_list': candidate_list}
	return render(request, 'candidate-archive.html', context)